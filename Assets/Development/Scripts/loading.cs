﻿using UnityEngine;
using System.Collections;

public class loading : MonoBehaviour {

	public static int loadlevel = 1;

	void Start () {
		Time.timeScale = 1;
		StartCoroutine("LoadCoroutine");
	}
	
	IEnumerator LoadCoroutine(){
		yield return new WaitForSeconds(2);
		Application.LoadLevelAsync(loadlevel);
	}

	void OnApplicationQuit()
	{
		loadlevel = 1;
	}
}
