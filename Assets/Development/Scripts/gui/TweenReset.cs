﻿using UnityEngine;
using System.Collections;

public class TweenReset : MonoBehaviour {

	public bool TweenScale = false;
	public bool TweenPosition = false;
	public bool TweenAlpha = false;


	void OnEnable()
	{
		if (TweenScale)
		{
			gameObject.GetComponent<TweenScale>().Reset();
			gameObject.GetComponent<TweenScale>().PlayForward();
		}

		if (TweenPosition)
			gameObject.GetComponent<TweenPosition>().Reset();

		if (TweenAlpha)
			gameObject.GetComponent<TweenAlpha>().Reset();
	}

}
