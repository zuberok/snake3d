﻿using UnityEngine;
using System.Collections;

public class SetLevelAvaiable : MonoBehaviour {
	public GameObject passivePanel;
	public string nameOfPreviousLevel;
	// Use this for initialization
	void Start () {
		if(!PlayerPrefs.HasKey(nameOfPreviousLevel) || int.Parse((PlayerPrefs.GetString(nameOfPreviousLevel).Split()[1])) == 0)
		{
			NGUITools.SetActive(passivePanel, true);
			NGUITools.SetActive(gameObject, false);
		}
		// else
		// {
		// 	NGUITools.SetActive(passivePanel, false);
		// 	NGUITools.SetActive(gameObject, true);
		// }
	}
}
