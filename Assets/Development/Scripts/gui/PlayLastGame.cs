﻿using UnityEngine;
using System.Collections;

public class PlayLastGame : MonoBehaviour {

	public int stepOfVelocity;
	public int pointsForEating;
	void OnClick(){
		int level;
		switch(PlayerPrefs.GetString("Last played level"))
		{
			case "Barrier":
				level = 3;
				break;
			case "2Rooms":
				level = 6;
				break;
			case "Corners":
				level = 5;
				break;
			case "Hall":
				level = 7;
				break;
			case "Borders":
				level = 4;
				break;
			case "Survival":
				level = 2;
				break;
			default:			
				PlayerPrefs.SetInt("Step Of Velocity", stepOfVelocity);
				PlayerPrefs.SetInt("Points For Eating", pointsForEating);
				level = 2;
				break;
		}
		loading.loadlevel = level;
		Application.LoadLevel ("loading");
	}
}
