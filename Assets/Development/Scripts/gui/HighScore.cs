﻿using UnityEngine;
using System.Collections;

public class HighScore : MonoBehaviour {

	public UILabel One;
	public UILabel Two;
	public UILabel Three;
	public UILabel Four;
	public UILabel Five;


	// Use this for initialization
	void Start () {
		One.text = ""+PlayerPrefs.GetInt("1st");
		Two.text = ""+PlayerPrefs.GetInt("2nd");
		Three.text = ""+PlayerPrefs.GetInt("3rd");
		Four.text = ""+PlayerPrefs.GetInt("4th");
		Five.text = ""+PlayerPrefs.GetInt("5th");
	}
	
	void OnEnable()
	{
		One.text = ""+PlayerPrefs.GetInt("1st");
		Two.text = ""+PlayerPrefs.GetInt("2nd");
		Three.text = ""+PlayerPrefs.GetInt("3rd");
		Four.text = ""+PlayerPrefs.GetInt("4th");
		Five.text = ""+PlayerPrefs.GetInt("5th");
	}
}
