﻿using UnityEngine;
using System.Collections;

public class SetVolume : MonoBehaviour {

	public GameObject panelEnabled;
	public GameObject panelDisabled;
	static AudioSource player = GameController.musicPlayer;
	void Awake()
	{
		panelEnabled.active = PlayerPrefs.GetInt("Volume") == 1 ? true : false;
		panelDisabled.active = !panelEnabled.active;
	}
	void OnClick()
	{
		player = GameController.musicPlayer;
		if(player != null)
		{
			if(player.volume == 0f)
			{
				player.volume = 1f;
				panelEnabled.active = true;
				PlayerPrefs.SetInt("Volume", (int)player.volume);
			}
			else
			{
				player.volume = 0f;
				panelEnabled.active = false;
				PlayerPrefs.SetInt("Volume", (int)player.volume);
			}
			panelDisabled.active = !panelEnabled.active;
		}
	}
}
