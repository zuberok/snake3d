﻿using UnityEngine;
using System.Collections;

public class SetAudioEnabled : MonoBehaviour {

	void Awake()
	{
		if(audio != null && GameController.musicPlayer != null)
		{
			audio.volume = PlayerPrefs.GetInt("Sound");
		}
	}
}
