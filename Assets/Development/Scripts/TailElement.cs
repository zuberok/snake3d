﻿using UnityEngine;
using System.Collections.Generic;

public class TailElement : MonoBehaviour {
	public GameObject navigator;
	public GameObject tailPrefab;
	public GameObject destroyEffect;
	public char nextDirection;
	public Vector3 startPosition;
	public TailElement follower;
	public Vector3 goalPosition;
	public float speed;
	char currentDirection;
	GameController gameController;
	public Vector3 goalRotation;
	float _temp;
	void Awake()
	{
		follower = null;
		gameController = GameObject.Find("BackGround").GetComponent<GameController>();
		Invoke("RandomizeRotation", 0.1f);
	}
	public void RandomizeRotation()
	{
		if(!CompareTag("tailEnd"))
		{
			Vector3 _temp = new Vector3(Random.Range(0,4) * 90f, 0, 0);
			Debug.Log(_temp);
			transform.Find("Body_").transform.Find("Body_Block").transform.localEulerAngles = _temp;
		}
		goalRotation = transform.eulerAngles =  navigator.transform.eulerAngles;
	}
	void FixedUpdate()
	{
		if(DistanceForNavigator() > 0.95f)
		{
			Move();
		}
		Rotate();
	}
	
	public void GrowUp()
	{
		if(follower != null && follower.CompareTag("TailElement"))
		{
			follower.GrowUp();
		}
		else
		{
			TailElement _temp = ((GameObject)Instantiate(gameObject, transform.position, transform.rotation)).GetComponent<TailElement>();
			_temp.follower = this.follower;
			follower.navigator = _temp.gameObject;
			follower = _temp;
			follower.navigator = this.gameObject;
		}
	}
	float DistanceForNavigator()
	{
		float result = Mathf.Abs(transform.position.x - navigator.transform.position.x) + Mathf.Abs(transform.position.y - navigator.transform.position.y);
		
		return result;
	}

	void Rotate()
	{
		if(goalRotation.z == 270f && transform.eulerAngles.z ==0f)
		{
			goalRotation.z = 359f;
			transform.eulerAngles = goalRotation;
			goalRotation.z = 270f;
		}

		transform.eulerAngles = Vector3.MoveTowards(transform.eulerAngles, goalRotation, gameController.snakeSpeed * 40f * Time.deltaTime);
		if(goalRotation.z == 360f && transform.eulerAngles.z > 350f)
		{
			goalRotation.z = 0f;
			transform.eulerAngles = goalRotation;
		}
	}
	void Move()
	{
		transform.position = Vector3.MoveTowards(transform.position, goalPosition, gameController.snakeSpeed * Time.deltaTime);
		if(transform.position == goalPosition)
		{
			UpdateGoalPosition(nextDirection);
		}
	}
	public void Kill()
	{
		if(follower != null)
		{
			follower.Kill();
		}
		Instantiate(destroyEffect, transform.position, Quaternion.identity);		
		Destroy(gameObject);
	}
	public void GrowTail()
	{
		TailElement _temp = ((GameObject)Instantiate(tailPrefab, transform.position, transform.rotation)).GetComponent<TailElement>();
		_temp.navigator = this.gameObject;
		follower = _temp;
		follower.goalPosition = this.goalPosition;
		follower.startPosition = this.startPosition;
		follower.speed = this.speed;
	}
	
	void UpdateGoalPosition(char dir)
	{

		if(goalPosition.x == gameController.maximalCoordinates.x + 1f)
		{
			goalPosition.x = gameController.minimalCoordinates.x;
			startPosition.x = goalPosition.x - 1f;
			transform.position = startPosition;
		}
		else if(goalPosition.x == gameController.minimalCoordinates.x - 1f)
		{
			goalPosition.x = gameController.maximalCoordinates.x;
			startPosition.x = goalPosition.x + 1f;
			transform.position = startPosition;
		}
		else if(goalPosition.y == gameController.maximalCoordinates.y + 1f)
		{
			goalPosition.y = gameController.minimalCoordinates.y;
			startPosition.y = goalPosition.y - 1f;
			transform.position = startPosition;
		}
		else if(goalPosition.y == gameController.minimalCoordinates.y - 1f)
		{
			goalPosition.y = gameController.maximalCoordinates.y;
			startPosition.y = goalPosition.y + 1f;
			transform.position = startPosition;
		}
		else
		{
			startPosition = goalPosition;
			if(Mathf.Abs(navigator.transform.position.x - transform.position.x) < 0.2f)
			{
				if(DistanceForNavigator() < 1.2f)
				{
					if (navigator.transform.position.y > transform.position.y)
					{
						goalPosition.y += 1f;
					}
					else
					{
						goalPosition.y -=1f;
					}
				}
				else
				{
					if(transform.position.y > 0)
					{
						goalPosition.y += 1f;
					}
					else if(transform.position.y < 0)
					{
						goalPosition.y -= 1f;
					}
				}
			}
			else if(Mathf.Abs(navigator.transform.position.y - transform.position.y) < 0.2f)
			{
				if(DistanceForNavigator() < 1.2f)
				{
					if (navigator.transform.position.x > transform.position.x)
					{
						goalPosition.x += 1f;
					}
					else
					{
						goalPosition.x -=1f;
					}
				}
				else
				{
					if(transform.position.x > 0)
					{
						goalPosition.x += 1f;
					}
					else if(transform.position.x < 0)
					{
						goalPosition.x -= 1f;
					}
				}
			}
		}
		if(follower != null && follower.goalRotation != this.goalRotation)
		{
			if(!(goalRotation.z == 0 && follower.goalRotation.z == 360f))
			{	follower.goalRotation = this.goalRotation;
			}
		}
	}
}
