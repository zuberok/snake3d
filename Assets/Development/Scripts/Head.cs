using UnityEngine;
using System.Collections.Generic;

public class Head : MonoBehaviour {

	public GameController gameController;
	public char nextDirection;

	TailElement follower;
	float speed = 5f;
	public char currentDirection;
	public Vector3 goalPosition;
	Vector3 startPosition;
	public Vector3 goalRotation;
	public AudioClip destroyclip;
	private int lengthOfTail = 0;
	public int tailCount
	{
		get
		{
			return lengthOfTail;
		}
	}
	void Start () 
	{
		UpdateGoalPosition(nextDirection);
		follower = null;
		Invoke("GrowUp",0.1f);
		Invoke("GrowUp",0.3f);
	}
	void FixedUpdate()
	{
		Move();
		Rotate();
	}
	int RecountLength()
	{
		int result = 1;
		TailElement _te = follower;
		while(_te.follower != null)
		{
			_te = _te.follower;
			++result;
		}
		return result;
	}
	public void GrowUp()
	{
		++lengthOfTail;
		if(follower == null)
		{
			follower = ((GameObject)Instantiate(gameController.tailPrefab,transform.position, transform.rotation)).GetComponent<TailElement>();

			follower.GetComponent<TailElement>().goalPosition = this.goalPosition;
			follower.GetComponent<TailElement>().startPosition = this.startPosition;
			follower.navigator = this.gameObject;
		}
		else
		{
			if(follower.CompareTag("tailEnd"))
			{
				TailElement _temp = ((GameObject)Instantiate(gameController.bodyPrefab, follower.transform.position, follower.transform.rotation)).GetComponent<TailElement>();
				_temp.follower = this.follower;
				this.follower.navigator = _temp.gameObject;
				_temp.navigator = this.gameObject;
				_temp.startPosition = this.follower.startPosition;
				_temp.goalPosition = this.follower.goalPosition;
				this.follower = _temp;

			}
			else if(follower.CompareTag("TailElement"))
			{
				follower.GrowUp();
			}
		}
	}
	void Move()
	{
		transform.position = Vector3.MoveTowards(transform.position, goalPosition, gameController.snakeSpeed * Time.deltaTime);
		if(transform.position == goalPosition)
		{
			UpdateGoalPosition(nextDirection);
		}
	}
	void UpdateGoalPosition(char dir)
	{
		if(follower != null)
		{
			follower.nextDirection = currentDirection;
		}
		startPosition = goalPosition;
		if(startPosition.x > gameController.maximalCoordinates.x)
		{
			startPosition.x = gameController.minimalCoordinates.x - 1f;
			transform.position = startPosition;
			Instantiate(gameController.teleportPrefab,startPosition, Quaternion.identity);
			goalPosition.x = gameController.minimalCoordinates.x;
		}
		else if(startPosition.x < gameController.minimalCoordinates.x)
		{
			startPosition.x = gameController.maximalCoordinates.x + 1f;
			transform.position = startPosition;
			Instantiate(gameController.teleportPrefab,startPosition, Quaternion.identity);
			goalPosition.x = gameController.maximalCoordinates.x;
		}
		else if(startPosition.y > gameController.maximalCoordinates.y)
		{
			startPosition.y = gameController.minimalCoordinates.y - 1f;
			transform.position = startPosition;
			Instantiate(gameController.teleportPrefab,startPosition, Quaternion.identity);
			goalPosition.y = gameController.minimalCoordinates.y;
		}
		else if(startPosition.y < gameController.minimalCoordinates.y)
		{
			
			startPosition.y = gameController.maximalCoordinates.y + 1f;
			transform.position = startPosition;
			Instantiate(gameController.teleportPrefab,startPosition, Quaternion.identity);
			goalPosition.y = gameController.maximalCoordinates.y;
		}
		else
		{
			switch (dir)
			{
				case 'r':
					goalPosition.x = Mathf.Round(goalPosition.x += 1f);
					break;
				case 'l':
					goalPosition.x = Mathf.Round(goalPosition.x -= 1f);
					break;
				case 'u':
					goalPosition.y = Mathf.Round(goalPosition.y += 1f);
					break;
				case 'd':
					goalPosition.y = Mathf.Round(goalPosition.y -= 1f);
					break;
				default:
					Debug.Log("Incorrect direction");
					break;
			}
			if(currentDirection != dir)
			{
				UpdateGoalRotation(dir);
				currentDirection = dir;
				if(follower != null)
				{
					follower.goalRotation = this.goalRotation;
				}
			}
		}
		
	}
	void Rotate()
	{
		transform.eulerAngles = Vector3.MoveTowards(transform.eulerAngles, goalRotation, 120 * speed * Time.deltaTime);
		if(transform.eulerAngles.z < 5f) // для поворота с 270 до 360, иначе он каждый раз при 360 сбрасывает на 0 и голова начинает бесконечно вращаться
		{
			Vector3 _temprot = new Vector3(0, 0, 0);
			transform.eulerAngles = _temprot;
			goalRotation.z = 0;
		}
	}
	void UpdateGoalRotation(char dir)
	{
		switch(dir)
		{
			case 'r':
				goalRotation.z = 180f;
				break;
			case 'l':
				goalRotation.z = 0f;
				if(transform.eulerAngles.z == 270f)
				{
					goalRotation.z = 360f;
				}
				break;
			case 'u':
				if(transform.eulerAngles.z == 0f)
				{
					goalRotation.z = 359f;
					transform.eulerAngles = goalRotation;
				}
				goalRotation.z = 270f;
				break;
			case 'd':
				goalRotation.z = 90f;
				break;
		}
	}
	public void Kill()
	{
		if(follower != null)
		{
			follower.Kill();
		}
		Destroy(gameObject);
	}
	void OnTriggerEnter(Collider col)
	{
		switch(col.tag)
		{
			case "Food":
				gameController.FoodEated();
				break;
			case "TailElement":
				if(col.GetComponent<TailElement>() != follower)
				{
					try{
						col.GetComponent<TailElement>().navigator.GetComponent<TailElement>().navigator.GetComponent<TailElement>().GrowTail();
						col.GetComponent<TailElement>().navigator.GetComponent<TailElement>().Kill();
						if(PlayerPrefs.GetInt("Sound") > 0)
						{
							audio.clip =  this.destroyclip;
							audio.Play();
						}
						gameController.Damage();
						GameController.Vibrate();
						lengthOfTail = RecountLength();
					}
					catch{}
				}
				break;
			case "tailEnd":
				if(col.GetComponent<TailElement>() != follower)
				{
					try{
						col.GetComponent<TailElement>().navigator.GetComponent<TailElement>().navigator.GetComponent<TailElement>().GrowTail();
						col.GetComponent<TailElement>().navigator.GetComponent<TailElement>().Kill();
						if(PlayerPrefs.GetInt("Sound") > 0)
						{
							audio.clip =  this.destroyclip;
							audio.Play();
						}
						gameController.Damage();
						GameController.Vibrate();
						lengthOfTail = RecountLength();
					}
					catch{}
				}
				break;
			case "Chance":
				gameController.ChanceObtained();
				break;
			case "Border":
				gameController.Damage();
				Debug.Log(col);
				break;
		}
	}
}
