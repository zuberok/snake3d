﻿using UnityEngine;
using System.Collections;

public class OpenPanel : MonoBehaviour {
	public GameObject panelToOpen;
	void OnClick()
	{
		NGUITools.SetActive(panelToOpen,true);
	}
}
