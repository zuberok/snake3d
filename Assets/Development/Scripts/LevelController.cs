﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;

public class LevelController : GameController
{
	public int[] scoresForStars;
	int numberOfStars = 0;
	public GameObject starSlider;
	public GameObject lineSlider;
	public override void ChanceObtained() {	}
 	public override void CompareAndSaveResult()
	{
		if((PlayerPrefs.HasKey(nameOfLevel) && score > int.Parse(PlayerPrefs.GetString(nameOfLevel).Split()[0])) || !PlayerPrefs.HasKey(nameOfLevel))
		{
			PlayerPrefs.SetString(nameOfLevel, score + " " + numberOfStars);
		}
	}
	public override void GiveChance()
	{
		chance = (GameObject)Instantiate(chancePrefab, FindFreeSpace(), chancePrefab.transform.rotation);
	}
	protected override void IncreaseScore(int n)
	{
		int _temp = score;
		score += n;

		scoreDisplay.text = ""+score;
		for(int i = 0; i < scoresForStars.Length; ++i)
		{
			if(score >= scoresForStars[i])
			{
				numberOfStars = i + 1;
			}
			else
			{
				break;
			}
		}
		UpdateStarSlider();
	}
	void UpdateStarSlider()
	{
		if(numberOfStars == 0)
		{
			lineSlider.GetComponent<UISprite>().fillAmount = (float)score / (3 * scoresForStars[0]);
		}
		else if(numberOfStars == 1)
		{
			lineSlider.GetComponent<UISprite>().fillAmount = 0.34f + (float)(score - scoresForStars[0]) / ((scoresForStars[1] - scoresForStars[0]) * 3);
		}
		else if(numberOfStars == 2)
		{
			lineSlider.GetComponent<UISprite>().fillAmount = 0.66f + (float)(score - scoresForStars[1]) / ((scoresForStars[2] - scoresForStars[1]) * 3);
		}
		else
		{
			lineSlider.GetComponent<UISprite>().fillAmount = 1f;
		}
		starSlider.GetComponent<UISprite>().fillAmount = 0.33f * numberOfStars;
	}
	void Start () 	
	{
		Instantiate(teleportPrefab, snakeInitializePosition, Quaternion.identity);
		lastMealPosition = snakeInitializePosition;
		currentDigit = countdownDigits[0];
		UpdateStarSlider();
		score = 0;
		var playmusic = GameObject.FindGameObjectWithTag ("music").GetComponent<music>();
		playmusic.audio.loop = true;
		playmusic.audio.Stop ();
		playmusic.ingamemusic ();
		if(!PlayerPrefs.HasKey("LEVELS_FINISHED"))
		{
			PlayerPrefs.SetInt("LEVELS_FINISHED", 0);
		}
		if (PlayerPrefs.GetInt("tutorsaw") == 0)
		{
			OpenTutorial();
		}
		else
		{
			CountDown();
		}
	}
	void CheckStarObtaining()
	{
		for(int i = 0; i < scoresForStars.Length; ++i)
		{
			if(score >= scoresForStars[i])
			{
				numberOfStars = i + 1;
			}
		}
	}
	public override void Damage()
	{
		PlayerPrefs.SetInt("LEVELS_FINISHED", PlayerPrefs.GetInt("LEVELS_FINISHED") + 1);
		GameController.Vibrate();
		resultPanel.transform.Find("Progress Bar").GetComponent<UISlider>().value = (float)numberOfStars / 3;
		finalScoreText.text = score.ToString();
		if((PlayerPrefs.HasKey(nameOfLevel) && score > int.Parse(PlayerPrefs.GetString(nameOfLevel).Split()[0])) || (!PlayerPrefs.HasKey(nameOfLevel)))
		{
			resultPanel.transform.Find("Sprite (New_Record)").active = true;
			PlayerPrefs.SetString(nameOfLevel, score + " " + numberOfStars);
		}
		else
		{
			resultPanel.transform.Find("Sprite (New_Record)").active = false;
		}
		inv = !PlayerPrefs.HasKey("Rated") && PlayerPrefs.GetInt("LEVELS_FINISHED") % 3 == 0;
		Die();
	}
	public override void FoodEated()
	{
		eatedFood++;
		IncreaseScore (pointsForEating);
		Destroy(currentFood);
		InitializeMeal();
		head.GrowUp();
	}
}
