﻿using UnityEngine;
using System.Collections;

public class OnClickListener : MonoBehaviour 
{
	GameController gameController;
	bool swipeUsed;
	Vector2 startPos;
	Vector2 endPos;
	public float minDelta = 15f;
	bool pressed;
	void Start()
	{
		gameController = GetComponent<GameController>();
		pressed = false;
	}
	
	void Update()
	{
		if(Input.GetMouseButton(0))
		{
			if(!pressed)
			{
				startPos = Input.mousePosition;
				pressed = true;
			}
			endPos = Input.mousePosition;
			Vector2 swipeVector = endPos - startPos;
			if(gameController.tutorial != null && gameController.tutorial.active && Mathf.Abs(swipeVector.x) + Mathf.Abs(swipeVector.y) > minDelta)
			{
				gameController.CloseTutorial();
				swipeUsed = true;
			}

			if(gameController.head != null && !swipeUsed && swipeVector.magnitude > minDelta)
			{
				switch(gameController.head.currentDirection)
				{
					case 'r':
						if(swipeVector.y > 0)
						{
							gameController.head.nextDirection = 'u';
						}
						else
						{
							gameController.head.nextDirection = 'd';
						}
						break;
					case 'l':
						if(swipeVector.y > 0)
						{
							gameController.head.nextDirection = 'u';
						}
						else
						{
							gameController.head.nextDirection = 'd';
						}
						break;
					case 'u':
						if(swipeVector.x > 0)
						{
							gameController.head.nextDirection = 'r';
						}
						else
						{
							gameController.head.nextDirection = 'l';
						}		
						break;
					case 'd':
						if(swipeVector.x > 0)
						{
							gameController.head.nextDirection = 'r';
						}
						else
						{
							gameController.head.nextDirection = 'l';
						}
						break;
				}
				swipeUsed = true;
			}
		}
		else
		{
			pressed = false;
			swipeUsed = false;
			if(Input.GetKey(KeyCode.UpArrow) && gameController.head.currentDirection != 'd')
			{
				gameController.head.nextDirection = 'u';
			}
			else if(Input.GetKey(KeyCode.DownArrow) && gameController.head.currentDirection != 'u')
			{
				gameController.head.nextDirection = 'd';
			}
			else if(Input.GetKey(KeyCode.LeftArrow) && gameController.head.currentDirection != 'r')
			{
				gameController.head.nextDirection = 'l';
			}
			else if(Input.GetKey(KeyCode.RightArrow) && gameController.head.currentDirection != 'l')
			{
				gameController.head.nextDirection = 'r';
			}
		}
	}
}
