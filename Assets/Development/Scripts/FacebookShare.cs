﻿using UnityEngine;
using System.Collections;

public class FacebookShare : MonoBehaviour {
	private const string FB_APP_ID = "1405769366363639";
    private const string FB_SHARE_URL = "http://www.facebook.com/dialog/feed";
    private const string ICON_URL = "http://cs608923.vk.me/v608923747/73f8/ymDX7pqT22Q.jpg";
    public const string GooglePlayUrl = "https://play.google.com/store/apps/details?id=kg.GameStudioParadise.ClassicSnakeMJQ";
	void OnClick()
	{
		Share();
	}
	void Share()
    {
        int maxLength = 300;

        Application.OpenURL(FB_SHARE_URL +
            "?app_id=" + FB_APP_ID +
            "&link=" + WWW.EscapeURL(GooglePlayUrl) +
            "&picture=" + WWW.EscapeURL(ICON_URL) +
            "&name=" + WWW.EscapeURL("Classic Snake Maya Jewel Quest") +
            "&caption=" + WWW.EscapeURL("Try the game on Google Play!") +
            "&description=" + WWW.EscapeURL("Just try and fun") +
            "&redirect_uri=" + WWW.EscapeURL("http://facebook.com"));
    }
}
