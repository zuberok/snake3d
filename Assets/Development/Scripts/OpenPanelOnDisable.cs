﻿using UnityEngine;
using System.Collections;

public class OpenPanelOnDisable : MonoBehaviour {

	public GameObject panel;

	void OnDisable()
	{
		NGUITools.SetActive(panel, false);
	}
}
