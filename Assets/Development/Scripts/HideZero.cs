﻿using UnityEngine;
using System.Collections;

public class HideZero : MonoBehaviour {
	public GameObject[] panelsToClose;
	// Use this for initialization
	void Start () {
		if(GetComponent<UILabel>().text == "0")
		{
			foreach(GameObject g in panelsToClose)
			{
				NGUITools.SetActive(g, false);
			}
		}
	}
	
}
