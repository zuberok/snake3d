﻿using UnityEngine;
using System.Collections;

public class TeleportController : MonoBehaviour {
	GameController gameController;
	Collider[] colliders;
	float timer;
	GameObject pair;
	void Start()
	{
		gameController = GameObject.Find("BackGround").GetComponent<GameController>();
		timer = 0f;
		Vector3 pairPosition = transform.position;
		if(transform.position.x == gameController.maximalCoordinates.x + 1f)
		{
			pairPosition.x = gameController.minimalCoordinates.x - 1f;
		}
		else if(transform.position.x == gameController.minimalCoordinates.x - 1f)
		{
			pairPosition.x = gameController.maximalCoordinates.x + 1f;
		}
		else if(transform.position.y == gameController.maximalCoordinates.y + 1f)
		{
			pairPosition.y = gameController.minimalCoordinates.y- 1f;
		}
		else if(transform.position.y == gameController.minimalCoordinates.y- 1f)
		{
			pairPosition.y = gameController.maximalCoordinates.y + 1f;
		}
		pair = (GameObject)Instantiate(gameObject, pairPosition, Quaternion.identity);
		Destroy(pair.GetComponent<TeleportController>());
	}
	void Update()
	{
		timer += Time.deltaTime;
	}
	void FixedUpdate()
	{
		colliders = Physics.OverlapSphere(transform.position, 0.8f);
		if(gameController.head != null && !IsThereSnake())
		{
			Destroy(gameObject);
			Destroy(pair.gameObject);
		}

	}
	void OnTriggerExit(Collider col){
		if(col.CompareTag("tailEnd"))
		{
			Destroy(gameObject);
		}
		
	}
	bool IsThereSnake()
	{
		bool result = false;
		foreach (Collider col in colliders)
		{
			if(!col.CompareTag("Teleport") && !col.CompareTag("Border"))
			{
				result = true;
			}
		}
		return result;
	}
}