﻿using UnityEngine;
using System.Collections;

public class musicdelete : MonoBehaviour {

	public GameObject globalSource;
	public AudioClip menuClip;
	void Awake()
	{
		if(GameController.musicPlayer == null)
		{
			GameController.musicPlayer = ((GameObject)Instantiate(globalSource)).GetComponent<AudioSource>();
		}
		GameController.musicPlayer.clip = menuClip;
	}
			
}
