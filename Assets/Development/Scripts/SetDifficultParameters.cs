﻿using UnityEngine;
using System.Collections;

public class SetDifficultParameters : MonoBehaviour {

	public int pointsForEating;
	public int stepOfVelocity;
	void OnClick()
	{
		loading.loadlevel = 2;
		Application.LoadLevel ("loading");
		PlayerPrefs.SetInt("Step Of Velocity", stepOfVelocity);
		PlayerPrefs.SetInt("Points For Eating", pointsForEating);
	}
}
