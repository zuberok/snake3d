﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;

public class SurvivalController : GameController
{
	public GameObject lifesDisplay;	
	int stepOfVelocity;
	UISlider lifeongui;
	public Material[] backgrounds;
	public int scoreForRateInvoking;
	float lifes = 0.33f;
	public GameObject eyePrefab;

	void Start () 	
	{
		Instantiate(teleportPrefab, snakeInitializePosition, Quaternion.identity);
		int _t = UnityEngine.Random.Range(0, backgrounds.Length);
		if(_t == 0)
		{
			Instantiate(eyePrefab, eyePrefab.transform.position, eyePrefab.transform.rotation);
		}
		renderer.material = backgrounds[_t];
		stepOfVelocity = PlayerPrefs.GetInt("Step Of Velocity");
		pointsForEating = PlayerPrefs.GetInt("Points For Eating");
		lastMealPosition = snakeInitializePosition;
		if(!PlayerPrefs.HasKey("1st"))
		{
			PlayerPrefs.SetInt("1st",0);
			PlayerPrefs.SetInt("2nd",0);
			PlayerPrefs.SetInt("3rd",0);
			PlayerPrefs.SetInt("4th",0);
			PlayerPrefs.SetInt("5th",0);
		}
		highScore = new int[]{PlayerPrefs.GetInt("1st"), PlayerPrefs.GetInt("2nd"),PlayerPrefs.GetInt("3rd"),PlayerPrefs.GetInt("4th"),PlayerPrefs.GetInt("5th")};
		currentDigit = countdownDigits[0];


		lifes = 0.33f;
		score = 0;
		lifeongui = lifesDisplay.GetComponent<UISlider>();
		lifeongui.value = lifes;
		
		try{
		var playmusic = GameObject.FindGameObjectWithTag ("music").GetComponent<music>();
		playmusic.audio.loop = true;
		playmusic.audio.Stop ();
		playmusic.ingamemusic ();}
		catch{}

		if (PlayerPrefs.GetInt("tutorsaw") == 0)
		{
			OpenTutorial();
		 }
		else
		{
			CountDown();
		}
	}	
	protected override void IncreaseScore(int n)
	{
		int _temp = score;
		score += n;
		scoreDisplay.text = score.ToString();
		if(stepOfVelocity != 0 && _temp / stepOfVelocity != score / stepOfVelocity)
		{
			snakeSpeed += 1f;
		}
		else
		{
			Debug.Log(score + " " + _temp);
		}
	}	
	public override void GiveChance()
	{
		chance = (GameObject)Instantiate(chancePrefab, FindFreeSpace(), chancePrefab.transform.rotation);
	}
	public override void ChanceObtained()
	{
		Destroy(chance);
		if(lifes < 1f)
		{
			this.lifes += 0.33f;
		}
		InitializeMeal();
		eatedFood++;
		lifeongui.value = lifes;
	}
	public override void CompareAndSaveResult()
	{
		if(score > highScore[highScore.Length - 1])
		{
			highScore[highScore.Length - 1] = score;
		}
		Array.Sort(highScore);
		PlayerPrefs.SetInt("1st",highScore[4]);
		PlayerPrefs.SetInt("2nd",highScore[3]);
		PlayerPrefs.SetInt("3rd",highScore[2]);
		PlayerPrefs.SetInt("4th",highScore[1]);
		PlayerPrefs.SetInt("5th",highScore[0]);
		
		finalScoreText.text = score.ToString();
	}
	public override void Damage()
	{
		lifes -= 0.33f;
		lifeongui.value = lifes;

		if(lifes < 0f)	{
			inv = !PlayerPrefs.HasKey("Rated") && score > scoreForRateInvoking;
			
			GameController.Vibrate();
			Die();
		}
	}
	public override void FoodEated()
	{
		eatedFood++;
		IncreaseScore (pointsForEating + head.tailCount);

		Destroy(currentFood);
		
		if(eatedFood > 1 && eatedFood % 10 == 0 && lifes < 0.8f)
		{
			GiveChance();
		}
		else
		{
			InitializeMeal();
		}
		head.GrowUp();
	}
}
