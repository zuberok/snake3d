﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;

public abstract class GameController : MonoBehaviour 
{
	public string nameOfLevel;
	public float snakeSpeed;
	public Vector2 minimalCoordinates;
	public Vector2 maximalCoordinates;
	public GameObject headPrefab;
	public GameObject tailPrefab;
	public GameObject bodyPrefab;
	public GameObject[] foodPrefabs;
	public UILabel scoreDisplay;
	public GameObject resultPanel;
	public GameObject tutorial;
	public UILabel finalScoreText;
	public GameObject[] countdownDigits;
	public Head head;
	public GameObject gameOverTable;
	public Vector3 snakeInitializePosition;
	public int startDirection;
	public int pointsForEating;
	public GameObject chancePrefab;
	public GameObject teleportPrefab;
	public GameObject pauseButton;
	public static AudioSource musicPlayer;
	public GameObject rateInvokePanel;
	public float gameOverPanelDuration;


	protected GameObject currentDigit;	
	protected GameObject currentFood;
	protected Vector3 lastMealPosition;
	protected int score;
	protected int eatedFood = 0;
	protected int[] highScore;
	protected GameObject chance;	
	protected bool inv;

	public static void Vibrate()
	{
		if(PlayerPrefs.GetInt("Vibration") > 0)
		{
			iPhoneUtils.Vibrate();
		}
	}
	protected void InvokeToRate()
	{
		NGUITools.SetActive(rateInvokePanel, true);
	}
	public  void CountDown()
	{
		ShowDigit();
		Invoke("HideDigit", 0.66f);
	}
	void Awake()
	{
		scoreDisplay.text = "0";
	}
	void ShowDigit()
	{
		NGUITools.SetActive(currentDigit, true);
	}
	void HideDigit()
	{
		NGUITools.SetActive(currentDigit, false);
		try{
			currentDigit = countdownDigits[Array.IndexOf(countdownDigits, currentDigit) + 1];
			ShowDigit();
			Invoke("HideDigit", 0.66f);
		}
		catch
		{
			InitializeSnake();
		}
	}
	protected void InitializeSnake()
	{
		head = ((GameObject)Instantiate(headPrefab, snakeInitializePosition, headPrefab.transform.rotation)).GetComponent<Head>();
		head.goalRotation = new Vector3(0,0,startDirection * 90f);
		head.goalPosition = snakeInitializePosition;
		switch(startDirection)
		{
			case 0:				
				head.goalPosition.x -= 1f;
				head.nextDirection = 'l';
				break;
			case 1:
				head.goalPosition.y -= 1f;
				head.nextDirection = 'd';
				break;
			case 2:				
				head.goalPosition.x += 1f;
				head.nextDirection = 'r';
				break;
			case 3:
				head.goalPosition.y += 1f;
				head.nextDirection = 'u';
				break;
			default:
			Debug.LogError("Invalid start direction of snake. Variable startDirection in Game Controller should be in range (0, 3)");
				break;
		}
		head.gameController = this;
		InitializeMeal();
	}
	protected void OpenTutorial()
	{
		PlayerPrefs.SetInt("tutorsaw", 1);
		NGUITools.SetActive(tutorial, true);
	}
	public void CloseTutorial()
	{
		NGUITools.SetActive(tutorial, false);
		CountDown();
		Destroy(tutorial);
	}
	protected abstract void IncreaseScore(int n);
	protected void InitializeMeal()
	{
		lastMealPosition = FindFreeSpace();
		int n = UnityEngine.Random.Range(0, foodPrefabs.Length);
		currentFood = (GameObject)Instantiate(foodPrefabs[n], lastMealPosition, foodPrefabs[n].transform.rotation);
	}
	public abstract void CompareAndSaveResult();
	void OnApplicationPause(bool isPause)
	{
		if(isPause)
		{
			CompareAndSaveResult();
		}
	}
	void OnApplicationQuit()
	{
		CompareAndSaveResult();
	}
	protected void Die()
	{
		finalScoreText.text = score.ToString();
		CompareAndSaveResult();
		head.Kill();
		StartCoroutine ("DieDelay");
		var playmusic = GameObject.FindGameObjectWithTag("music").GetComponent<music>();
		playmusic.audio.Stop ();
		playmusic.GameOverMusic ();
		
		Destroy(pauseButton.collider);
	}
	public abstract void ChanceObtained();
	protected IEnumerator DieDelay()
	{
		yield return new WaitForSeconds (1);
		NGUITools.SetActive (gameOverTable, true);

		yield return new WaitForSeconds(gameOverPanelDuration);
		// var playmusic = GameObject.FindGameObjectWithTag ("music").GetComponent<music>();
		// playmusic.audio.Stop ();
		// playmusic.inmenumusic ();

		yield return new WaitForSeconds (2);
		if(inv)
		{
			InvokeToRate();
			Debug.Log("INVOKED");
		}
		else
		{
			Debug.LogWarning("INVITING: " + inv);
		}
		NGUITools.SetActive (gameOverTable, false);
		GameController.Vibrate();
		if(nameOfLevel == "Survival" || !inv){	NGUITools.SetActive (resultPanel, true);}

	}
	public abstract void Damage();
	public abstract void FoodEated();
	public abstract void GiveChance();
	protected Vector3 FindFreeSpace()
	{
		Vector3 position = new Vector3(0, 0, 0);
		Collider[] colliders;
		int count = 1;
		while(count != 0)
		{
			position.x = UnityEngine.Random.Range(Mathf.RoundToInt(minimalCoordinates.x), Mathf.RoundToInt(maximalCoordinates.x));
			position.y = UnityEngine.Random.Range(Mathf.RoundToInt(minimalCoordinates.y), Mathf.RoundToInt(maximalCoordinates.y));
			colliders = Physics.OverlapSphere(position, 0.5f);
			count = colliders.Length;
		}

		return position;
	}
}
