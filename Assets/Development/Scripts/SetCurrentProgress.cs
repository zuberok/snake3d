﻿using UnityEngine;
using System.Collections;

public class SetCurrentProgress : MonoBehaviour {
	public string levelName;
	// Use this for initialization
	void Start () {
		if(PlayerPrefs.HasKey(levelName))
		{
			string s = PlayerPrefs.GetString(levelName);
			transform.Find("Label").GetComponent<UILabel>().text = s.Split()[0];
			transform.Find("Progress Bar").GetComponent<UISlider>().value = 0.34f * int.Parse(s.Split()[1]);
		}
		else
		{
			transform.Find("Label").GetComponent<UILabel>().text = "new";
			transform.Find("Progress Bar").GetComponent<UISlider>().value = 0;
		}
	}
}
