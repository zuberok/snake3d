﻿using UnityEngine;
using System.Collections;

public class GoToMarket : MonoBehaviour {
	
	public string marketURL = "https://play.google.com/store/apps/details?id=kg.GameStudioParadise.ClassicSnakeMJQ";
	void OnClick()
	{
		gameObject.active = false;
		PlayerPrefs.SetInt("Rated",1);
		Application.OpenURL(marketURL);
	}
}
