﻿using UnityEngine;
using System.Collections;

public class StartCountdownAfterClick : MonoBehaviour {

	void OnClick()
	{
		GameObject.Find("BackGround").GetComponent<GameController>().CountDown();
	}
}
