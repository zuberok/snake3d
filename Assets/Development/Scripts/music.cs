﻿using UnityEngine;
using System.Collections;

public class music : MonoBehaviour {

	public AudioClip Game;
	public AudioClip Menu;
	public AudioClip GameOver;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (gameObject);
		audio.clip = Menu;
		PlayTheme();
		if(!PlayerPrefs.HasKey("Volume"))
		{
			PlayerPrefs.SetInt("Volume", 1);
			PlayerPrefs.SetInt("Sound", 1);
			PlayerPrefs.SetInt("Vibration",1);
		}
		GameController.musicPlayer.volume = PlayerPrefs.GetInt("Volume");
	}
	
	public void ingamemusic()
	{
		audio.clip = Game;
		PlayTheme();
	}

	public void inmenumusic()
	{
		audio.clip = Menu;
		PlayTheme();
	}

	public void GameOverMusic(){
		audio.clip = GameOver;
		PlayTheme();
	}
	void PlayTheme()
	{
		audio.Play();
	}

}
