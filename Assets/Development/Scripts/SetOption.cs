﻿using UnityEngine;
using System.Collections;

public class SetOption : MonoBehaviour {
	public GameObject panelEnabled;
	public GameObject panelDisabled;
	public string option;
	void Awake()
	{
		if(!PlayerPrefs.HasKey(option))
		{
			PlayerPrefs.SetInt(option, 1);
		}
		SetIcon();
	}
	void OnClick()
	{
		PlayerPrefs.SetInt(option, PlayerPrefs.GetInt(option) == 0 ? 1 : 0);
		SetIcon();
	}
	void SetIcon()
	{
		panelEnabled.active = PlayerPrefs.GetInt(option) == 1;
		panelDisabled.active = !panelEnabled.active;
	}
}
