﻿using UnityEngine;
using System.Collections;

public class spawnrotate : MonoBehaviour {

	public string startanim;
	public string rotateanim;

	IEnumerator Start()
	{
		animation.Play(startanim);
		animation.PlayQueued(rotateanim);
		yield return new WaitForSeconds(1);
	}
}
