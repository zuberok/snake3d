using UnityEngine;
using System.Collections;
using StartApp;

public class StartAppBackAndHomePlugin : MonoBehaviour{

	public static bool Init = false;

	void Start () {
		if (!Init)
		{
			StartAppWrapper.loadAd();
			DontDestroyOnLoad(gameObject);
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			Init = true;
		}else
		{
			Destroy(gameObject);
		}
    }
	void Update () {
		if (Input.GetKeyUp(KeyCode.Escape) == true ) {
			 showAdAndExit();
		}
    }
	void OnApplicationPause(bool pauseStatus) {
		if (pauseStatus) {
			StartAppWrapper.doHome();
		} else {
			StartAppWrapper.loadAd();
		}
    }
	
    void showAdAndExit() {	
		StartAppWrapper.showAdAndExit();
	}
}