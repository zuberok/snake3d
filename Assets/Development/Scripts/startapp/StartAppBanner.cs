﻿using UnityEngine;
using System.Collections;
using StartApp;

public class StartAppBanner : MonoBehaviour {

	public static bool inited = false;

	// Use this for initialization
	void Start () {

		if (!inited)
		{
			StartAppWrapper.addBanner(
				StartAppWrapper.BannerType.AUTOMATIC,
				StartAppWrapper.BannerPosition.BOTTOM);
			inited = true;
		}
	}
}
