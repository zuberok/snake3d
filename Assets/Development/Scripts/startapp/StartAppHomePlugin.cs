using UnityEngine;
using System.Collections;
using StartApp;

public class StartAppHomePlugin : MonoBehaviour {
	void Start () {
		StartAppWrapper.loadAd();
    }

	void OnApplicationPause(bool pauseStatus) {
		if (pauseStatus) {
			StartAppWrapper.doHome();
		} else {
			StartAppWrapper.loadAd();
		}
    }
}