﻿using UnityEngine;
using System.Collections;
using StartApp;

public class StartAppShower : MonoBehaviour {

	public static int showID = 0;

	// Use this for initialization
	void Start () {
		if (showID == 0) 
		{
			StartAppWrapper.showAd ();
			showID++;
		} 
		else 
		{
			showID = 0;
		}
	}
}
