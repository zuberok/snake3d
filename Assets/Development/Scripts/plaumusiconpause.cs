﻿using UnityEngine;
using System.Collections;

public class plaumusiconpause : MonoBehaviour {


	void OnEnable()
	{
		var playmusic = GameObject.FindGameObjectWithTag ("music").GetComponent<music>();
		playmusic.audio.Stop ();
		playmusic.inmenumusic ();
	}

	void OnDisable()
	{
		var playmusic = GameObject.FindGameObjectWithTag ("music").GetComponent<music>();
		playmusic.audio.Stop ();
		playmusic.ingamemusic ();
	}
}
