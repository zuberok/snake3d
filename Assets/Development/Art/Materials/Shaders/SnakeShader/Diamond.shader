// Shader created with Shader Forge Beta 0.25 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.25;sub:START;pass:START;ps:flbk:Reflective/Specular,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:True,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|diff-240-OUT,diffpow-12-OUT,spec-10-OUT,gloss-250-OUT,emission-17-OUT,lwrap-23-OUT,amspl-17-OUT,alpha-27-OUT;n:type:ShaderForge.SFN_Color,id:3,x:33326,y:32334,ptlb:Color,c1:0,c2:0.6275859,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:5,x:33441,y:32698,ptlb:Specular Color,c1:0.1323529,c2:0.3178498,c3:1,c4:1;n:type:ShaderForge.SFN_Slider,id:9,x:33296,y:32870,ptlb:Specular_Power,min:0,cur:2.347059,max:3;n:type:ShaderForge.SFN_Multiply,id:10,x:33124,y:32800|A-5-RGB,B-9-OUT;n:type:ShaderForge.SFN_Slider,id:12,x:33091,y:32637,ptlb:Diffuse_Power,min:0,cur:1.195726,max:2;n:type:ShaderForge.SFN_Cubemap,id:13,x:33520,y:33034,ptlb:IBL,cube:51e3f49dbd110a7011005aba44295342,pvfc:0;n:type:ShaderForge.SFN_Slider,id:15,x:33456,y:33209,ptlb:IBL_Power,min:0,cur:1.001544,max:2;n:type:ShaderForge.SFN_Multiply,id:17,x:33174,y:32997|A-13-RGB,B-15-OUT;n:type:ShaderForge.SFN_Multiply,id:23,x:33174,y:33164|A-13-RGB,B-25-OUT;n:type:ShaderForge.SFN_Slider,id:25,x:33456,y:33301,ptlb:Emiss_Power,min:0,cur:1.460809,max:2;n:type:ShaderForge.SFN_Slider,id:27,x:32870,y:33325,ptlb:Alpha,min:0,cur:0.798865,max:1;n:type:ShaderForge.SFN_Fresnel,id:236,x:33326,y:32478;n:type:ShaderForge.SFN_Lerp,id:240,x:32980,y:32494|A-3-RGB,B-3-R,T-236-OUT;n:type:ShaderForge.SFN_Slider,id:250,x:33628,y:32604,ptlb:Glossy,min:0,cur:1,max:2;proporder:3-5-9-12-13-15-25-27-250;pass:END;sub:END;*/

Shader "Shader Forge/Diamond" {
    Properties {
        _Color ("Color", Color) = (0,0.6275859,1,1)
        _SpecularColor ("Specular Color", Color) = (0.1323529,0.3178498,1,1)
        _SpecularPower ("Specular_Power", Range(0, 3)) = 2.347059
        _DiffusePower ("Diffuse_Power", Range(0, 2)) = 1.195726
        _IBL ("IBL", Cube) = "_Skybox" {}
        _IBLPower ("IBL_Power", Range(0, 2)) = 1.001544
        _EmissPower ("Emiss_Power", Range(0, 2)) = 1.460809
        _Alpha ("Alpha", Range(0, 1)) = 0.798865
        _Glossy ("Glossy", Range(0, 2)) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform float4 _SpecularColor;
            uniform float _SpecularPower;
            uniform float _DiffusePower;
            uniform samplerCUBE _IBL;
            uniform float _IBLPower;
            uniform float _EmissPower;
            uniform float _Alpha;
            uniform float _Glossy;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float3 normalDirection =  i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float4 node_13 = texCUBE(_IBL,viewReflectDirection);
                float3 w = (node_13.rgb*_EmissPower)*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = pow(max(float3(0.0,0.0,0.0), NdotLWrap + w ), _DiffusePower);
                float3 diffuse = forwardLight * attenColor + UNITY_LIGHTMODEL_AMBIENT.xyz;
////// Emissive:
                float3 node_17 = (node_13.rgb*_IBLPower);
                float3 emissive = node_17;
///////// Gloss:
                float gloss = exp2(_Glossy*10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float3 specularColor = (_SpecularColor.rgb*_SpecularPower);
                float3 specularAmb = node_17 * specularColor;
                float3 specular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),gloss) * specularColor + specularAmb;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float4 node_3 = _Color;
                finalColor += diffuseLight * lerp(node_3.rgb,float3(node_3.r,node_3.r,node_3.r),(1.0-max(0,dot(normalDirection, viewDirection))));
                finalColor += specular;
                finalColor += emissive;
/// Final Color:
                return fixed4(finalColor,_Alpha);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform float4 _SpecularColor;
            uniform float _SpecularPower;
            uniform float _DiffusePower;
            uniform samplerCUBE _IBL;
            uniform float _IBLPower;
            uniform float _EmissPower;
            uniform float _Alpha;
            uniform float _Glossy;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                LIGHTING_COORDS(2,3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float3 normalDirection =  i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float4 node_13 = texCUBE(_IBL,viewReflectDirection);
                float3 w = (node_13.rgb*_EmissPower)*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = pow(max(float3(0.0,0.0,0.0), NdotLWrap + w ), _DiffusePower);
                float3 diffuse = forwardLight * attenColor;
///////// Gloss:
                float gloss = exp2(_Glossy*10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float3 specularColor = (_SpecularColor.rgb*_SpecularPower);
                float3 specular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),gloss) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float4 node_3 = _Color;
                finalColor += diffuseLight * lerp(node_3.rgb,float3(node_3.r,node_3.r,node_3.r),(1.0-max(0,dot(normalDirection, viewDirection))));
                finalColor += specular;
/// Final Color:
                return fixed4(finalColor * _Alpha,0);
            }
            ENDCG
        }
    }
    FallBack "Reflective/Specular"
    CustomEditor "ShaderForgeMaterialInspector"
}
