// Shader created with Shader Forge Beta 0.25 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.25;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:False,hqlp:True,blpr:0,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:True,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0;n:type:ShaderForge.SFN_Final,id:1,x:32555,y:32653|diff-88-OUT,diffpow-11-OUT,spec-18-OUT,gloss-128-OUT,normal-137-OUT,transm-372-OUT,amspl-160-OUT;n:type:ShaderForge.SFN_Tex2d,id:2,x:33543,y:32385,ptlb:Diffuse Map,tex:f6f92a608c959b54b8bd960ad63ab2a3,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:9,x:33402,y:32929,ptlb:Glossiness,min:0,cur:0.715567,max:2;n:type:ShaderForge.SFN_Slider,id:11,x:32974,y:32614,ptlb:Diffuse Power,min:0.01,cur:0.8237879,max:2.4;n:type:ShaderForge.SFN_Blend,id:18,x:33014,y:32704,blmd:12,clmp:True|SRC-2-RGB,DST-75-OUT;n:type:ShaderForge.SFN_Slider,id:20,x:33435,y:32616,ptlb:Specular Power,min:0,cur:0.9649082,max:2;n:type:ShaderForge.SFN_Color,id:72,x:33480,y:32745,ptlb:Specular Color,c1:0.5514706,c2:0.5514706,c3:0.5514706,c4:1;n:type:ShaderForge.SFN_Multiply,id:75,x:33200,y:32725|A-20-OUT,B-72-RGB,C-2-A;n:type:ShaderForge.SFN_Color,id:86,x:33131,y:32439,ptlb:DiffuseColor,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:88,x:32948,y:32384|A-2-RGB,B-86-RGB;n:type:ShaderForge.SFN_Multiply,id:128,x:33061,y:32909|A-2-A,B-9-OUT;n:type:ShaderForge.SFN_Tex2d,id:130,x:33302,y:33038,ptlb:Normal Map,tex:13e5f91c8577f5244b1101aed9b9d2dd,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Normalize,id:137,x:33118,y:33038|IN-130-RGB;n:type:ShaderForge.SFN_Cubemap,id:144,x:33170,y:33284,ptlb:IBL,cube:0cd051eb21b63c745bf4ca30c1c30ab1,pvfc:4|DIR-351-XYZ;n:type:ShaderForge.SFN_Slider,id:158,x:32811,y:33384,ptlb:IBL Power,min:0,cur:0.3799527,max:1.4;n:type:ShaderForge.SFN_Multiply,id:160,x:32953,y:33125|A-144-RGB,B-158-OUT;n:type:ShaderForge.SFN_ObjectPosition,id:351,x:33423,y:33239;n:type:ShaderForge.SFN_Slider,id:370,x:32586,y:32379,ptlb:Transmissin,min:0.01,cur:0.8237879,max:2.4;n:type:ShaderForge.SFN_Multiply,id:372,x:32676,y:32499|A-370-OUT,B-18-OUT;proporder:86-11-2-130-20-72-9-144-158-370;pass:END;sub:END;*/

Shader "Shader Forge/Base_1" {
    Properties {
        _DiffuseColor ("DiffuseColor", Color) = (1,1,1,1)
        _DiffusePower ("Diffuse Power", Range(0.01, 2.4)) = 0.8237879
        _DiffuseMap ("Diffuse Map", 2D) = "white" {}
        _NormalMap ("Normal Map", 2D) = "black" {}
        _SpecularPower ("Specular Power", Range(0, 2)) = 0.9649082
        _SpecularColor ("Specular Color", Color) = (0.5514706,0.5514706,0.5514706,1)
        _Glossiness ("Glossiness", Range(0, 2)) = 0.715567
        _IBL ("IBL", Cube) = "_Skybox" {}
        _IBLPower ("IBL Power", Range(0, 1.4)) = 0.3799527
        _Transmissin ("Transmissin", Range(0.01, 2.4)) = 0.8237879
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _DiffuseMap; uniform float4 _DiffuseMap_ST;
            uniform float _Glossiness;
            uniform float _DiffusePower;
            uniform float _SpecularPower;
            uniform float4 _SpecularColor;
            uniform float4 _DiffuseColor;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform samplerCUBE _IBL;
            uniform float _IBLPower;
            uniform float _Transmissin;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_394 = i.uv0;
                float3 normalLocal = normalize(tex2D(_NormalMap,TRANSFORM_TEX(node_394.rg, _NormalMap)).rgb);
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 forwardLight = pow(max(0.0, NdotL ), _DiffusePower);
                float4 node_2 = tex2D(_DiffuseMap,TRANSFORM_TEX(node_394.rg, _DiffuseMap));
                float3 node_18 = saturate((node_2.rgb > 0.5 ?  (1.0-(1.0-2.0*(node_2.rgb-0.5))*(1.0-(_SpecularPower*_SpecularColor.rgb*node_2.a))) : (2.0*node_2.rgb*(_SpecularPower*_SpecularColor.rgb*node_2.a))) );
                float3 backLight = pow(max(0.0, -NdotL ), _DiffusePower) * (_Transmissin*node_18);
                float3 diffuse = (forwardLight+backLight) * attenColor + UNITY_LIGHTMODEL_AMBIENT.xyz;
///////// Gloss:
                float gloss = exp2((node_2.a*_Glossiness)*10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float3 specularColor = node_18;
                float3 specularAmb = (texCUBE(_IBL,objPos.rgb).rgb*_IBLPower) * specularColor;
                float3 specular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),gloss) * specularColor + specularAmb;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * (node_2.rgb*_DiffuseColor.rgb);
                finalColor += specular;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _DiffuseMap; uniform float4 _DiffuseMap_ST;
            uniform float _Glossiness;
            uniform float _DiffusePower;
            uniform float _SpecularPower;
            uniform float4 _SpecularColor;
            uniform float4 _DiffuseColor;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _Transmissin;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_395 = i.uv0;
                float3 normalLocal = normalize(tex2D(_NormalMap,TRANSFORM_TEX(node_395.rg, _NormalMap)).rgb);
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 forwardLight = pow(max(0.0, NdotL ), _DiffusePower);
                float4 node_2 = tex2D(_DiffuseMap,TRANSFORM_TEX(node_395.rg, _DiffuseMap));
                float3 node_18 = saturate((node_2.rgb > 0.5 ?  (1.0-(1.0-2.0*(node_2.rgb-0.5))*(1.0-(_SpecularPower*_SpecularColor.rgb*node_2.a))) : (2.0*node_2.rgb*(_SpecularPower*_SpecularColor.rgb*node_2.a))) );
                float3 backLight = pow(max(0.0, -NdotL ), _DiffusePower) * (_Transmissin*node_18);
                float3 diffuse = (forwardLight+backLight) * attenColor;
///////// Gloss:
                float gloss = exp2((node_2.a*_Glossiness)*10.0+1.0);
////// Specular:
                NdotL = max(0.0, NdotL);
                float3 specularColor = node_18;
                float3 specular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),gloss) * specularColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * (node_2.rgb*_DiffuseColor.rgb);
                finalColor += specular;
/// Final Color:
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
